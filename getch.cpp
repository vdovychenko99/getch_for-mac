include <iostream>
#include <cstdio>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>

int getche()
{
    struct termios oldt, newt;
    int  ch;
    tcgetattr( STDIN_FILENO, &oldt );
    newt = oldt;
    newt.c_lflag &= ~( ICANON | ECHO );
    tcsetattr( STDIN_FILENO, TCSANOW, &newt );
    ch = std::getchar();
    tcsetattr( STDIN_FILENO, TCSANOW, &oldt );
    std::putchar(ch);
    return ch;
}

int main()
{
    using namespace std;

    int chcount = 0;
    int wdcount = 1;
    char ch='a';
    cout << "Enter text:";
    while(ch != '%')
    {
        ch = getche();
        if(ch == ' ')
            wdcount++;
        else
            chcount++;
    }
    cout << "\nWords:" << wdcount << endl
         << "Latters:" << (chcount-1)<<endl;
    return 0;
}
